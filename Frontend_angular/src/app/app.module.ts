import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HttpModule } from '@angular/http';
import { HttpService } from './providers/http/http.service';
import { AddrecipeComponent } from './components/addrecipe/addrecipe.component';
import { MatFormFieldModule, MatInputModule, MatAutocompleteModule, MatButtonModule, MatCheckboxModule, MatDialog, MatDialogModule, MatIconModule, MatListModule, MatIcon, MatChipsModule, MatSelectTrigger, MatSelectModule, MatSnackBar, MatSnackBarModule } from '@angular/material'
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogComponent } from './components/dialog/dialog.component';
import { CategoryComponent } from './components/category/category.component';
import { RecipepageComponent } from './components/recipepage/recipepage.component'
import { NgbModule, NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import "angular2-navigate-with-data";
import { RouterModule } from '@angular/router';
import { AuthGuard } from './providers/guards/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddrecipeComponent,
    DialogComponent,
    CategoryComponent,
    RecipepageComponent
  ],
  imports: [
    RouterModule,
    MatSnackBarModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    MatCheckboxModule,
    MatIconModule,
    MatListModule,
    MatChipsModule,
    MatSelectModule,
    NgbModule.forRoot()
  ],
  entryComponents: [
    AddrecipeComponent,
    DialogComponent
  ],
  providers: [
    HttpService,
    NgbCarouselConfig,
    AuthGuard
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
