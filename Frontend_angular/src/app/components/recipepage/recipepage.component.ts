import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-recipepage',
  templateUrl: './recipepage.component.html',
  styleUrls: ['./recipepage.component.css']
})
export class RecipepageComponent implements OnInit {
  servings = ['2', '4', '6', '8'];
  recipe = [];
  clickedRecipe: any;
  ingByServings = [];
  previousServings: any;
  oldIngredientAmount: any;
  newIngredientAmount: any;
  nutrients = [];
  convertValue: any;
  totalNutrients = {
    protein: 0,
    kolhydrater: 0,
    energi: 0,
    fett: 0,
    mattatFett: 0,
    omattatFett: 0,
    fleroOmattatFett: 0,
  };

  constructor() { }

  ngOnInit() {
    this.clickedRecipe = JSON.parse(localStorage.getItem('clickedRecipe'));
    this.recipe.push(this.clickedRecipe);
    this.getNutrients();
  }

  getPreviousServings(servings) {
    this.previousServings = servings;
  }

  calculateIngredients(newServings) {
    if (this.previousServings && newServings)
      this.recipe.map(res => {
        res.Ingredienser.forEach(res => {
          this.oldIngredientAmount = res.amount;
          this.newIngredientAmount = (this.oldIngredientAmount * newServings) / this.previousServings;
          res.amount = Math.round(this.newIngredientAmount * 10) / 10;
        });
      });
  }

  parseToInt() {
    this.recipe[0].Ingredienser.forEach(data => {
      data.Naringsvarden.forEach(value => {
        value.Varde = parseFloat(value.Varde);
      });
    });
  }

  getNutrients() {
    this.parseToInt();
    this.recipe.forEach(res => {
      res.Ingredienser.forEach(ing => {
        if (ing.unitInGrams) {
          this.convertValue = ing.unitInGrams / parseInt(ing.ViktGram);
        } else {
          this.convertValue = ing.amount / parseInt(ing.ViktGram);
        }
        ing.Naringsvarden.forEach(nut => {
          this.convertValue = parseInt(this.convertValue);
          if (nut.Namn == 'Protein') {
            this.totalNutrients.protein += this.convertValue * nut.Varde / 4;
          }
          if (nut.Namn == 'Kolhydrater') {
            this.totalNutrients.kolhydrater += this.convertValue * nut.Varde / 4;
          }
          if (nut.Namn == 'Energi (kcal)') {
            this.totalNutrients.energi += this.convertValue * nut.Varde / 4;
          }
          if(nut.Namn == 'Fett'){
            this.totalNutrients.fett += this.convertValue * nut.Varde / 4;
          }
          if(nut.Forkortning == 'Mfet'){
            this.totalNutrients.mattatFett += this.convertValue * nut.Varde / 4;
          }
          if(nut.Forkortning == 'Mone'){
            this.totalNutrients.omattatFett += this.convertValue * nut.Varde / 4;
          }
          if(nut.Forkortning == 'Pole'){
            this.totalNutrients.fleroOmattatFett += this.convertValue * nut.Varde / 4;
          }
        });
      })
    });
  }
}






