import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../../providers/recipe/recipe.service';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router, } from '@angular/router';
import { NgbCarouselConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../providers/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  myControl = new FormControl();
  form: FormGroup;
  userInput: any;
  allRecipes = [];
  filteredRecipes = [];
  username: any;
  password: any;

  constructor(public _recipeService: RecipeService, public _router: Router, _carouselConfig: NgbCarouselConfig, private _fb: FormBuilder, private _modalService: NgbModal, private _authService: AuthService) {
    this.form = this._fb.group({
      username: ['', [Validators.required, Validators.minLength(2)]],
      password: ['', [Validators.required, Validators.minLength(2)]],
    });
    _carouselConfig.interval = 3500;
    _carouselConfig.pauseOnHover = true;
    this.getAllRecipes();
  }

  ngOnInit() { }

  getAllRecipes() {
    this._recipeService.getAllRecipes().subscribe(res => {
      this.allRecipes = res.json();
      console.log('allRecipes', this.allRecipes);
    });
  }

  autocompleteRecipes(userInput) {
    if (userInput.length >= 2) {
      this._recipeService.filterRecipeByInput(userInput).subscribe(res => {
        this.filteredRecipes = res.json();
      });
    }
  }

  clearOptions(userInput) {
    if (userInput.length < 2) {
      this.filteredRecipes = [];
    }
  }

  recipeDetail(recipe) {
    this._router.navigateByData({
      url: ['/recipe-details', recipe._id],
      data: [recipe]
    });
    localStorage.setItem('clickedRecipe', JSON.stringify(recipe));
  }

  openModal(content) {
    this._modalService.open(content, { ariaLabelledBy: 'modal-header', centered: true }).result.then((result) => {
      return result;
    }, (reason) => {
      return reason;
    })
  }

  goToAddRecipe() {
    this._authService.checkAuth().subscribe(res => {
      res = res.json();
      if (res[0].username.toLowerCase() == this.username.toLowerCase()) {
        if (res[0].password.toLowerCase() == this.password.toLowerCase()) {
          this._router.navigateByData({
            url: ['/addrecipe'],
            data: 0
          })
        };
      }
    })
  }
}
