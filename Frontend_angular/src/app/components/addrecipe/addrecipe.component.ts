import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Observable } from 'rxjs';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { RecipeService } from '../../providers/recipe/recipe.service';

@Component({
  selector: 'app-addrecipe',
  templateUrl: './addrecipe.component.html',
  styleUrls: ['./addrecipe.component.css']
})
export class AddrecipeComponent implements OnInit {

  myControl = new FormControl();
  categories = new FormControl();
  form: FormGroup;
  filteredOptions: Observable<any[]>;
  selectedIngredients: string[] = [];
  allIngredients: string[] = [];
  recipeTitle: any;
  instructions: any;
  amount: any;
  unit: any;
  imgUrl: any;
  selectedTags: any[] = [];
  selectedFile = null;
  categoryList: string[] = ['Asiatiskt', 'Bakverk', 'Fisk',
    'Fläsk', 'Fågel', 'Grekiskt', 'Husmanskost',
    'Indiskt', 'Italienskt', 'Nöt', 'Pasta', 'Smoothies', 'Spanskt', 'Vegetariskt'];

  constructor(private _recipeService: RecipeService, private dialog: MatDialog, private _fb: FormBuilder, private _snackBar: MatSnackBar) {
    this.form = this._fb.group({
      title: ['', [Validators.required, Validators.minLength(2)]],
      instructions: ['', [Validators.required, Validators.minLength(2)]],
      tags: ['', [Validators.required, Validators.minLength(1)]],
      imgUrl: ['', [Validators.required, Validators.minLength(2)]]
    });
  }

  ngOnInit() { }

  getIngredients(userInput) {
    if (userInput.length >= 2) {
      this._recipeService.filterIngredientByInput(userInput).subscribe(res => {
        this.allIngredients = res.json();
      });
    }
  }

  onOptionSelected(ingredient) {
    let dialogRef = this.dialog.open(DialogComponent, {
      width: '300px',
      data: { selectedIngredient: ingredient }

    });
    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        ingredient.amount = result.amount;
        ingredient.unit = result.unit;
        ingredient.unitInGrams = result.unitInGrams;
      }
      if (ingredient.unitInGrams) {
        ingredient.unitInGrams = ingredient.unitInGrams * ingredient.amount;
      }
    })
    this.selectedIngredients.push(ingredient);
  }

  removeIngredient(index) {
    this.selectedIngredients.splice(index, 1);
  }

  clearOptions(userInput) {
    if (userInput.length < 2) {
      this.allIngredients = [];
    }
  }

  createNewRecipe() {
    let newRecipe = {
      Titel: this.recipeTitle,
      Personer: 4,
      Instruktioner: [this.instructions],
      Ingredienser: this.selectedIngredients,
      Taggar: this.selectedTags,
      bildURL: this.imgUrl
    };
    if (this.form.valid) {
      this._snackBar.open('Ditt recept har sparats och är nu publicerat', 'x', {
        duration: 3500
      });
      this._recipeService.createNewRecipe(newRecipe).subscribe(res => console.log(res));
    }
  }
}

