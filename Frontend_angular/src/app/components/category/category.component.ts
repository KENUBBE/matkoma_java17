import { Component, OnInit } from '@angular/core';
import { RecipeService } from '../../providers/recipe/recipe.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categoryList: string[] = ['Visa alla', 'Asiatiskt', 'Bakverk', 'Fisk',
    'Fläsk', 'Fågel', 'Grekiskt', 'Husmanskost',
    'Indiskt', 'Italienskt', 'Nöt', 'Pasta', 'Smoothies', 'Spanskt', 'Vegetariskt'];
  allRecipes: any[] = [];
  filteredRecipes: any[] = [];
  showAll = 'Visa alla';

  constructor(private _recipeService: RecipeService, private _router: Router) {};

  ngOnInit() {
    this.getAllRecipes();
  }

  getAllRecipes() {
    this._recipeService.getAllRecipes()
      .subscribe(res => {
        this.allRecipes = res.json();
        this.toggleRecipe(this.showAll);
      });
  }

  toggleRecipe(category) {
    if (category == this.showAll) {
      this.filteredRecipes = this.allRecipes;
    } else {
      this.filteredRecipes = this.allRecipes.filter(res => res.Taggar.includes(category));
    }
  }

  recipeDetail(recipe) {
    this._router.navigateByData({
      url: ['/recipe-details', recipe._id],
      data: [recipe]
    });
    localStorage.setItem('clickedRecipe', JSON.stringify(recipe));
  }
}
