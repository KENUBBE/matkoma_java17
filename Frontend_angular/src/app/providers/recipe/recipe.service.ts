import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  constructor(private _httpService: HttpService) { }

  public filterIngredientByInput(value) {
    return this._httpService.get(`/api/ingredient-name/${value}`);
  }

  public filterRecipeByInput(value) {
    return this._httpService.get(`/api/recipe-name/${value}`);
  }

  public createNewRecipe(value) {
    return this._httpService.post('/api/addrecipe', value);
  }

  public getRecipeByCategoryOrTag(value) {
    return this._httpService.get(`/api/getRecipeByCategoryOrTag/${value}`);
  }

  public getAllRecipes() {
    return this._httpService.get('/api/allRecipes');
  }
}
