import { Injectable } from '@angular/core';
import { HttpService } from '../http/http.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private _httpService: HttpService) { }

  public checkAuth (){
    return this._httpService.get('/api/admin/');
  }
}
