import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { baseApiUrl } from '../../contants/constants';

@Injectable()
export class HttpService {

  constructor(public http: Http) { }

  public get(endpoint: string): Observable<any> {
    return this.http.get(baseApiUrl + endpoint);
  };

  public post(endpoint: string, body: any): Observable<any> {
    return this.http.post(baseApiUrl + endpoint, body);
  };
}


