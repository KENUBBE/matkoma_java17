
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AddrecipeComponent } from './components/addrecipe/addrecipe.component';
import { CategoryComponent } from './components/category/category.component';
import { RecipepageComponent } from './components/recipepage/recipepage.component';
import { AuthGuard } from './providers/guards/auth.guard';


const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'addrecipe', component: AddrecipeComponent, canActivate: [AuthGuard]},
    { path: 'categories', component: CategoryComponent },
    { path: 'recipe-details/:id', component: RecipepageComponent}
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}
