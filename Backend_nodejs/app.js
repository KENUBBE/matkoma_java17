// Require the express module
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

app.use(cors());
app.use(express.static('www'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.listen(3000, () => console.log('Listening on port 3000'));

//Require the mongoose module
const mongoose = require('mongoose');
const mongoDB = 'mongodb://localhost:27017/Matkoma';

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.log('Connected to mongoDB');
})

let ldata = require('./json/livsmedelsdata.json');

mongoose.connect(mongoDB, { useNewUrlParser: true });

const IngredientSchema = new mongoose.Schema({
    Nummer: {},
    Namn: {},
    ViktGram: {},
    Huvudgrupp: {},
    Naringsvarden: {}
});

const RecipeSchema = new mongoose.Schema({
    Titel: {},
    Personer: Number,
    Instruktioner: {},
    Ingredienser: {},
    Taggar: {},
    bildURL: String
});

const UserAccount = new mongoose.Schema({
    username: String,
    password: String
});

let Ingredient = mongoose.model('Ingredients', IngredientSchema);
let Recipe = mongoose.model('Recipes', RecipeSchema);
let Admin = mongoose.model('Admins', UserAccount);

// Get admin from db
app.get('/api/admin/', (req, res) => {
    Admin.find({})
    .then(doc => {
        if (!doc) {
            return res.status(404).end();
        } else {
            return res.status(200).json(doc);
        }
    }).catch(err => res.json(err));
})

// Filter ingredients by input from user
app.get('/api/ingredient-name/:userInput', (req, res) => {
    let userInput = req.params.userInput;
    if (userInput.length < 2) {
        res.send({ error: 'Please provide at least two characters...' });
        return;
    }

    Ingredient.find({ Namn: new RegExp('^' + userInput, 'i') })
        .then(doc => {
            if (!doc) {
                return res.status(404).end();
            } else {
                return res.status(200).json(doc);
            }
        }).catch(err => res.json(err));
}),

    // Filter recipes by input from user
    app.get('/api/recipe-name/:userInput', (req, res) => {
        let userInput = req.params.userInput;
        if (userInput.length < 2) {
            res.send({ error: 'Please provide at least two characters...' });
            return;
        }
        Recipe.find().or([{Titel: new RegExp(userInput, 'i')}, {Instruktioner: new RegExp(userInput, 'i')}])
            .then(doc => {
                if (!doc) {
                    return res.status(404).end();
                } else {
                    return res.status(200).json(doc);
                }
            }).catch(err => res.json(err));
    }),
    //Add new recipe to db
    app.post('/api/addrecipe', (req, res) => {
        let recipe = new Recipe(req.body);
        recipe.save()
            .then(recipe => {
                res.send('recipe saved to database');
            })
            .catch(err => {
                res.status(400).send('unable to save to database');
            })
    }),

    //Get all ingredients from db
    app.get('/api/allRecipes/', (req, res) => {
        Recipe.find({})
            .then(doc => {
                if (!doc) {
                    return res.status(404).end();
                } else {
                    return res.status(200).json(doc);
                }
            }).catch(err => res.json(err));
    })

    // Creating db structure and pushing data from livsmedel.json to mongodb collection
    //
    //     ldata.forEach(item => {
    //       const livsmedel = new Ingredient({
    //         Nummer: item.Nummer,
    //         Namn: item.Namn,
    //         ViktGram: item.ViktGram,
    //         Huvudgrupp: item.Huvudgrupp,
    //         Naringsvarden: []
    //       });
    //       item.Naringsvarden.Naringsvarde.forEach(item => {
    //         livsmedel.Naringsvarden.push({
    //           Namn: item.Namn ? item.Namn : undefined,
    //           Forkortning: item.Forkortning ? item.Forkortning : undefined,
    //           Varde: item.Varde ? item.Varde : undefined,
    //           Enhet: item.Enhet ? item.Enhet : undefined,
    //           SenastAndrad: item.SenastAndrad ? item.SenastAndrad : undefined,
    //           Vardetyp: item.Vardetyp ? item.Vardetyp : undefined,
    //           Ursprung: item.Ursprung ? item.Ursprung : undefined,
    //           Publikation: item.Publikation ? item.Publikation : undefined,
    //           Metodtyp: item.Metodtyp ? item.Metodtyp : undefined,
    //           Framtagningsmetod: item.Framtagningsmetod
    //             ? item.Framtagningsmetod
    //             : undefined,
    //           Referenstyp: item.Referenstyp ? item.Referenstyp : undefined
    //         });
    //       });
    //       livsmedel.markModified([
    //         'Nummer',
    //         'Namn',
    //         'ViktGram',
    //         'Huvudgrupp',
    //         'Naringsvarden'
    //       ]);
    //       livsmedel.save();
    // });


